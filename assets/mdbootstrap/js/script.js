$(document).ready(() => {
  // SideNav Initialization
  $(".button-collapse").sideNav();

  new WOW().init();

  // Show Search
  $('.form-search').hide();
  $('.search-button').click(function(){
  	$('.form-search').fadeIn();
  })
  $('.close-search-button').click(function(){
  	$('.form-search').fadeOut();
  })
});

$(window).scroll(() => {
	let scrollVal = $(window).scrollTop();

	if (scrollVal >= 70) {
		$('.navbar').css('background', 'white');
		$('.logo').css('filter', 'brightness(0)');
		$('.nav-text').css('color', 'black');
		$('.button-collapse').removeClass('text-white');
		$('.button-collapse').addClass('text-dark');
		$('.fa-search').removeClass('text-white');
		$('.fa-search').addClass('text-dark');
	} else {
		$('.navbar').css('background', '');
		$('.logo').css('filter', '');
		$('.nav-text').css('color', 'white');
		$('.button-collapse').removeClass('text-dark');
		$('.button-collapse').addClass('text-white');
		$('.fa-search').removeClass('text-dark');
		$('.fa-search').addClass('text-white');
	}
})