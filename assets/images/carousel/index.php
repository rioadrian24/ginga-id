<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>
		Amazing text changing animation using css
	</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
	<div class="main">
		<h1>
			WE ARE
			<div>
				<ul>
					<?php for ($i = 0; $i < 100; $i++) : ?>
						<li>FULLYWORLD</li>
						<li>CREATIVE</li>
						<li>AMAZING</li>
					<?php endfor ?>
				</ul>
			</div>
		</h1>
		<p>DON'T FORGET TO SUBSCRIBE</p>
	</div>
</body>
</html>