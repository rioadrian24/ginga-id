<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Codeigniter</title>

	<style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		#container {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			font-style: italic;
		}

		.text-orange {
			color: #E13300;
		}
		
	</style>
</head>
<body>

	<div id="container">
		<h2>CODEIGNITER | <span class="text-orange">WELCOME</span></h2>
	</div>

</body>
</html>