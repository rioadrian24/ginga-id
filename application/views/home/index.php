<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Ginga ID | Official Website</title>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= base_url('assets/mdbootstrap/fontawesome/css/all.min.css') ?>">
	<!-- Bootstrap core CSS -->
	<link href="<?= base_url('assets/mdbootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="<?= base_url('assets/mdbootstrap/css/mdb.min.css') ?>" rel="stylesheet">
	<!-- Your custom styles (optional) -->
	<link href="<?= base_url('assets/mdbootstrap/css/style.css') ?>" rel="stylesheet">
</head>

<body class="fixed-sn">

	<!--Double navigation-->
	<header>
		<!-- Sidebar navigation -->
		<div id="slide-out" class="side-nav bg-white fixed">
			<ul class="custom-scrollbar">
				<li>
					<p class="waves-effect"><i class="fas fa-bars"></i> MENU</p>
				</li>
				<li>
					<a href="#" class="waves-effect">CITIES</a>
				</li>
				<li>
					<a href="#" class="waves-effect">MOMENTS</a>
				</li>
				<li>
					<a href="#" class="waves-effect">FOR BUSINESS</a>
				</li>
				<li>
					<a href="#" class="waves-effect">FOR PHOTOGRAPHERS</a>
				</li>
				<li>
					<a class="waves-effect">
						<img class="mb-1" src="<?= base_url('assets/images/logo/promo-jingle-deals.svg') ?>" alt="Promo Jingle Deals" width="90"></span>
					</a>
				</li>
				<li>
					<p class="waves-effect"><i class="fas fa-globe-asia"></i> LANGUAGE</p>
				</li>
				<!-- Side navigation links -->
				<li>
					<ul class="collapsible collapsible-accordion mt-0">
						<li>
							<a class="collapsible-header waves-effect arrow-r">Bahasa Indonesia<i class="fas fa-angle-down rotate-icon"></i></a>
							<div class="collapsible-body">
								<ul class="list-unstyled">
									<li>
										<a href="#" class="waves-effect pl-5">English</a>
									</li>
									<li>
										<a href="#" class="waves-effect pl-5">Bahasa Indonesia</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</li>
				<!--/. Side navigation links -->
			</ul>
			<div class="sidenav-bg mask-strong"></div>
		</div>
		<!--/. Sidebar navigation -->
		<!-- Navbar -->
		<nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
			<div class="container">
				<!-- SideNav slide-out button -->
				<div class="float-left">
					<a href="#" data-activates="slide-out" class="button-collapse black-text d-lg-none d-md-none"><i class="fas fa-bars button-collapse text-white"></i></a>
				</div>
				<!-- Breadcrumb-->
				<div class="breadcrumb-dn mx-auto mx-lg-0 mr-lg-3 d-block">
					<a href="<?= base_url() ?>">
						<img src="<?= base_url('assets/images/logo/logo-white.svg') ?>" class="img-fluid logo" alt="Logo">
					</a>
				</div>
				<ul class="nav navbar-nav nav-flex-icons d-block d-sm-none d-lg-none">
					<li class="nav-item">
						<a role="button" class="search-button p-2">
							<i class="fas fa-search text-white"></i>
						</a>
					</li>
				</ul>
				<ul class="nav navbar-nav nav-flex-icons mr-auto d-none d-lg-flex">
					<li class="nav-item">
						<a class="nav-link"><span class="clearfix d-none d-sm-inline-block nav-text">Cities</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link"><span class="clearfix d-none d-sm-inline-block nav-text">Moments</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link"><span class="clearfix d-none d-sm-inline-block nav-text">For Business</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link"><span class="clearfix d-none d-sm-inline-block nav-text">For Photographers</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link"><span class="clearfix d-none d-sm-inline-block nav-text"><img src="<?= base_url('assets/images/logo/promo-jingle-deals.svg') ?>" alt="Promo Jingle Deals" width="120"></span></a>
					</li>
				</ul>
				<ul class="nav navbar-nav nav-flex-icons ml-auto d-none d-lg-flex">
					<li class="nav-item">
						<a role="button" class="search-button p-2">
							<i class="fas fa-search text-white"></i>
						</a>
					</li>
				</ul>
			</div>
		</nav>
		<!-- /.Navbar -->

		<!-- Form Search -->
		<div class="container fixed-top form-search">
			<div class="row">
				<div class="col-lg-12">
					<div class="card shadow mt-2">
						<div class="card-body">
							<div class="row">
								<div class="col-1 col-lg-1">
									<a role="button" class="close-search-button"><i class="fas fa-times fa-lg mt-2"></i></a>
								</div>
								<div class="col-11 col-lg-11">
									<input type="text" class="form-control" placeholder="Searching ...">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.Form Search -->

		<!-- Carousel -->
		<div id="carouselControls" class="carousel slide carousel-fade" data-ride="carousel">
			<div class="carousel-text">
				<h1>MAKE U</h1>
				<h1>DIGITAL</h1>
			</div>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="<?= base_url('assets/images/carousel/slide1.jpg') ?>" alt="Slide 1">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="<?= base_url('assets/images/carousel/slide2.jpg') ?>" alt="Slide 2">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="<?= base_url('assets/images/carousel/slide3.jpg') ?>" alt="Slide 2">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="<?= base_url('assets/images/carousel/slide4.jpg') ?>" alt="Slide 2">
				</div>
			</div>
		</div>
		<!-- /.Carousel -->
	</header>
	<!--/.Double navigation-->

	<!--Main layout-->
	<main>

		<div class="container-fluid text-center">
			
		</div>

	</main>
	<!--/Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center text-md-left pt-4 bg-dark">

		<!--Footer Links-->
		<div class="container-fluid">
			<div class="row">

				<!--First column-->
				<div class="col-md-3">
					<h5 class="text-uppercase font-weight-bold mb-4">Footer Content</h5>
					<p>Here you can use rows and columns here to organize your footer content.</p>
				</div>
				<!--/.First column-->

				<hr class="w-100 clearfix d-md-none">

				<!--Second column-->
				<div class="col-md-2 mx-auto">
					<h5 class="text-uppercase font-weight-bold mb-4">Links</h5>
					<ul class="list-unstyled">
						<li><a href="#!">Link 1</a></li>
						<li><a href="#!">Link 2</a></li>
						<li><a href="#!">Link 3</a></li>
						<li><a href="#!">Link 4</a></li>
					</ul>
				</div>
				<!--/.Second column-->

				<hr class="w-100 clearfix d-md-none">

				<!--Third column-->
				<div class="col-md-2 mx-auto">
					<h5 class="text-uppercase font-weight-bold mb-4">Links</h5>
					<ul class="list-unstyled">
						<li><a href="#!">Link 1</a></li>
						<li><a href="#!">Link 2</a></li>
						<li><a href="#!">Link 3</a></li>
						<li><a href="#!">Link 4</a></li>
					</ul>
				</div>
				<!--/.Third column-->

				<hr class="w-100 clearfix d-md-none">

				<!--Fourth column-->
				<div class="col-md-2 mx-auto">
					<h5 class="text-uppercase font-weight-bold mb-4">Links</h5>
					<ul class="list-unstyled">
						<li><a href="#!">Link 1</a></li>
						<li><a href="#!">Link 2</a></li>
						<li><a href="#!">Link 3</a></li>
						<li><a href="#!">Link 4</a></li>
					</ul>
				</div>
				<!--/.Fourth column-->

			</div>
		</div>
		<!--/.Footer Links-->

		<hr>

		<!--Call to action-->
		<div class="call-to-action text-center my-4">
			<ul class="list-unstyled list-inline">
				<li class="list-inline-item">
					<h5>Register for free</h5>
				</li>
				<li class="list-inline-item"><a href="" class="btn btn-primary">Sign up!</a></li>
			</ul>
		</div>
		<!--/.Call to action-->

		<hr>

		<!--Social buttons-->
		<div class="social-section text-center">
			<ul class="list-unstyled list-inline">
				<li class="list-inline-item"><a class="btn-floating btn-fb"><i class="fab fa-facebook-f"> </i></a></li>
				<li class="list-inline-item"><a class="btn-floating btn-tw"><i class="fab fa-twitter"> </i></a></li>
				<li class="list-inline-item"><a class="btn-floating btn-gplus"><i class="fab fa-google-plus-g"> </i></a></li>
				<li class="list-inline-item"><a class="btn-floating btn-li"><i class="fab fa-linkedin-in"> </i></a></li>
				<li class="list-inline-item"><a class="btn-floating btn-git"><i class="fab fa-github"> </i></a></li>
			</ul>
		</div>
		<!--/.Social buttons-->

		<!--Copyright-->
		<div class="footer-copyright py-3 text-center">
			<div class="container-fluid">
				© 2018 Copyright: <a href="http://www.MDBootstrap.com"> MDBootstrap.com </a>

			</div>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="<?= base_url('assets/mdbootstrap/js/jquery-3.4.1.min.js') ?>"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="<?= base_url('assets/mdbootstrap/js/popper.min.js') ?>"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="<?= base_url('assets/mdbootstrap/js/bootstrap.min.js') ?>"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="<?= base_url('assets/mdbootstrap/js/mdb.min.js') ?>"></script>
	<!-- Custom JavaScript -->
	<script type="text/javascript" src="<?= base_url('assets/mdbootstrap/js/script.js') ?>"></script>
</body>

</html>