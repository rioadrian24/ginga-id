<?php defined('BASEPATH') OR exit('No direct script access allowed');

function post($name)
{
	$ci = get_instance();

	echo $ci->input->post(htmlspecialchars($name, ENT_QUOTES, 'UTF-8'));
}

function get($name)
{
	$ci = get_instance();

	echo $ci->input->post(htmlspecialchars($name, ENT_QUOTES, 'UTF-8'));
}

function filter($data)
{
	echo htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
}

function view($view, $data = [])
{
	$ci = get_instance();

	return $ci->load->view($view, $data);
}

function query($query)
{
	$ci = get_instance();

	if ($type == 'row') {
		echo $ci->db->query($query)->row();
	} elseif ($type == 'row_array') {
		echo $ci->db->query($query)->row_array();
	} elseif ($type == 'result') {
		echo $ci->db->query($query)->result();
	} elseif ($type == 'result_array') {
		echo $ci->db->query($query)->result_array();
	} else {
		echo 'Please select "row/row_array/result/result_array" in the second parameter.';
	}
}

function db_get($table, $type)
{
	$ci = get_instance();

	$table = htmlspecialchars($table, ENT_QUOTES, 'UTF-8');

	if ($type == 'row') {
		echo $ci->db->get($table)->row();
	} elseif ($type == 'row_array') {
		echo $ci->db->get($table)->row_array();
	} elseif ($type == 'result') {
		echo $ci->db->get($table)->result();
	} elseif ($type == 'result_array') {
		echo $ci->db->get($table)->result_array();
	} else {
		echo 'Please select "row/row_array/result/result_array" in the second parameter.';
	}
}

function db_getWhere($table, $where, $type)
{
	$ci = get_instance();

	$table = htmlspecialchars($table, ENT_QUOTES, 'UTF-8');

	if ($type == 'row') {
		echo $ci->db->get_where($table, $where)->row();
	} elseif ($type == 'row_array') {
		echo $ci->db->get_where($table, $where)->row_array();
	} elseif ($type == 'result') {
		echo $ci->db->get_where($table, $where)->result();
	} elseif ($type == 'result_array') {
		echo $ci->db->get_where($table, $where)->result_array();
	} else {
		echo 'Please select "row/row_array/result/result_array" in the second parameter.';
	}
}

function db_insert($table, $data)
{
	$ci = get_instance();

	$table = htmlspecialchars($table, ENT_QUOTES, 'UTF-8');

	echo $ci->db->insert($table, $data);
}

function db_set($field, $data) {
	$ci = get_instance();

	$field = htmlspecialchars($field, ENT_QUOTES, 'UTF-8');

	echo $ci->db->set($field, $data);
}

function db_where($field, $data) {
	$ci = get_instance();

	$field = htmlspecialchars($field, ENT_QUOTES, 'UTF-8');

	echo $ci->db->where($field, $data);
}

function db_update($field) {
	$ci = get_instance();

	$field = htmlspecialchars($field, ENT_QUOTES, 'UTF-8');

	echo $ci->db->update($field);
}

function db_delete($field) {
	$ci = get_instance();

	$field = htmlspecialchars($field, ENT_QUOTES, 'UTF-8');

	echo $ci->db->delete($field);
}

function validate($name, $rules, $message = null)
{
	$ci = get_instance();

	if ($message == null) {
		echo $ci->form_validation->set_rules($name, ucfirst($name), $rules);
	} else {
		echo $ci->form_validation->set_rules($name, ucfirst($name), $rules, $message);
	}
}

function run_validate()
{
	$ci = get_instance();

	echo $ci->form_validation->run();
}

function dateID()
{
	$day   = htmlspecialchars(date('d'));
	$days  = htmlspecialchars(date('N'));
	$year  = htmlspecialchars(date('Y'));
	$month = htmlspecialchars(date('m'));

	if ($days == 1) {
		$days = htmlspecialchars('Senin');
	} elseif ($days == 2) {
		$days = htmlspecialchars('Selasa');
	} elseif ($days == 3) {
		$days = htmlspecialchars('Rabu');
	} elseif ($days == 4) {
		$days = htmlspecialchars('Kamis');
	} elseif ($days == 5) {
		$days = htmlspecialchars('Jum\'at');
	} elseif ($days == 6) {
		$days = htmlspecialchars('Sabtu');
	} elseif ($days == 7) {
		$days = htmlspecialchars('Minggu');
	}

	if ($month == 1) {
		$month = htmlspecialchars('Januari');
	} elseif ($month == 2) {
		$month = htmlspecialchars('Februari');
	} elseif ($month == 3) {
		$month = htmlspecialchars('Maret');
	} elseif ($month == 4) {
		$month = htmlspecialchars('April');
	} elseif ($month == 5) {
		$month = htmlspecialchars('Mei');
	} elseif ($month == 6) {
		$month = htmlspecialchars('Juni');
	} elseif ($month == 7) {
		$month = htmlspecialchars('Juli');
	} elseif ($month == 8) {
		$month = htmlspecialchars('Agustus');
	} elseif ($month == 9) {
		$month = htmlspecialchars('September');
	} elseif ($month == 10) {
		$month = htmlspecialchars('Oktober');
	} elseif ($month == 11) {
		$month = htmlspecialchars('November');
	} elseif ($month == 12) {
		$month = htmlspecialchars('Desember');
	}

	echo $days . ", " . $day . " " . $month . " " . $year;
}

function timeID()
{
	$time = htmlspecialchars(time());

	echo date('H:i', $time);
}

function timesID()
{
	$time = htmlspecialchars(time());

	echo date('h:i', $time);
}